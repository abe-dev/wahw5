package com.abedev.wahw5;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

public class MainActivity extends AppCompatActivity implements Selector.Listener {
    private static final String KEY_LAST_ID = "lastId";
    private static final String KEY_ITEMS = "items";
    private static final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager = getLayoutManager();
    private SelectableAdapter adapter;
    private Selector selector;
    private Toolbar toolbar;
    private ActionMode actionMode;
    private final ActionMode.Callback actionModeCallback;
    private AtomicLong lastId = new AtomicLong(0);

    public MainActivity() {
        actionModeCallback = createActionModeCallback();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        selector = new Selector();
        selector.addListener(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        adapter = new SelectableAdapter(getLayoutInflater(),selector);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        if (savedInstanceState==null)
            for (int i = 0; i < 50; i++)
                adapter.addItem(createNewItem());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.itemAdd){
            adapter.addItem(createNewItem());
            return true;
        }
        else
            return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(KEY_LAST_ID,lastId.get());
        outState.putParcelableArrayList(KEY_ITEMS,adapter.getItems());
        selector.saveState(outState);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        selector.restoreState(savedInstanceState);
        lastId.set(savedInstanceState.getLong(KEY_LAST_ID));
        ArrayList<ValueWrapper> items = savedInstanceState.getParcelableArrayList(KEY_ITEMS);
        adapter.setItems(items);
    }

    @Override
    public void stateChanged() {
        if (selector.isInSelectMode() && actionMode == null) {
            startActionMode(actionModeCallback);
            Log.d(TAG, actionMode.toString());
        }
    }

    @Override
    public void selectionChanged(int selectedItemsCount) {
        if (actionMode!=null)
            setActionModeTitle(actionMode,selectedItemsCount);
    }

    private void setActionModeTitle(ActionMode actionMode, int selectedItemsCount) {
        actionMode.setTitle(String.format(getString(R.string.patternCelectedItemsCount),selectedItemsCount));
    }

    private ValueWrapper createNewItem() {
        long id = lastId.incrementAndGet();
        String text = String.format(Locale.getDefault(),"Item # %d",id);
        return new ValueWrapper(text,id);
    }

    @NonNull
    private ActionMode.Callback createActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                actionMode = mode;
                setActionModeTitle(mode,selector.getSelectedItemsIds().size());
                Log.d(TAG,String.format("onCreateActionMode activity %1$s mode %2$s",MainActivity.this.toString(),mode.toString()));
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.action_mode_menu,menu);
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.itemDelete) {
                    deleteSelectedItems(mode);
                }
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                Log.d(TAG,String.format("onDestroyActionMode activity %1$s mode %2$s",MainActivity.this.toString(),mode.toString()));
                selector.endSelection();
                actionMode = null;
            }
        };
    }

    private void deleteSelectedItems(ActionMode mode) {
        Set<Long> itemsToDelete = selector.getSelectedItemsIds();
        if (itemsToDelete.size() > 0) {
            final ArrayList<ValueWrapper> newItems = new ArrayList<>();
            for (ValueWrapper item : adapter.getItems()) {
                if (!itemsToDelete.contains(item.id))
                    newItems.add(item);
            }

            adapter.setItems(newItems);
            mode.finish();
        }
    }


    private RecyclerView.LayoutManager getLayoutManager() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return (position % 3 == 0 ? 2 : 1);
            }
        });
        return gridLayoutManager;
    }


}
