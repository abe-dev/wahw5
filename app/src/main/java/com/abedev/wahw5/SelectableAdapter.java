package com.abedev.wahw5;

import android.os.Bundle;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by home on 06.09.2016.
 */
public class SelectableAdapter extends RecyclerView.Adapter<SelectableViewHolder> implements Selector.Listener, SelectableViewHolder.ClicksListener{
    private ArrayList<ValueWrapper> items = new ArrayList<>();
    private final LayoutInflater inflater;
    private Selector selector;

    public SelectableAdapter(LayoutInflater inflater,Selector selector) {
        this.inflater = inflater;
        this.selector = selector;
        selector.addListener(this);
        setHasStableIds(true);
    }


    @Override
    public SelectableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new SelectableViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(SelectableViewHolder holder, int position) {
        ValueWrapper item = items.get(position);
        holder.setValue(item.value,item.id);
        holder.setSelection(selector.isInSelectMode(),selector.isSelected(item.id));
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).id;
    }

    @Override
    public int getItemViewType(int position) {
        return SelectableViewHolder.VIEW_TYPE;
    }

    public void setItems(ArrayList<ValueWrapper> newItems) {
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtilCallback(newItems));
        items = newItems;
        result.dispatchUpdatesTo(this);
    }

    public ArrayList<ValueWrapper> getItems() {
        return items;
    }

    public void addItem(ValueWrapper item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    @Override
    public void stateChanged() {
        notifyDataSetChanged();
    }

    @Override
    public void selectionChanged(int selectedItemsCount) {

    }

    @Override
    public void onClick(SelectableViewHolder viewHolder, long itemId) {
        if (selector.isInSelectMode()) {
            viewHolder.setSelection(true,selector.toggleSelection(itemId));
        }
        else
            Toast.makeText(inflater.getContext(),
                    String.format(inflater.getContext()
                            .getString(R.string.message_item_clicked),itemId),
                    Toast.LENGTH_SHORT)
                    .show();
    }

    @Override
    public void onLongClick(SelectableViewHolder viewHolder, long itemId) {
        if (!selector.isInSelectMode()) {
            selector.startSelection(itemId);
        }
    }

    class DiffUtilCallback extends DiffUtil.Callback {
        final ArrayList<ValueWrapper> newItems;

        public DiffUtilCallback(ArrayList<ValueWrapper> newItems) {
            this.newItems = newItems;
        }


        @Override
        public int getOldListSize() {
            return items.size();
        }

        @Override
        public int getNewListSize() {
            return newItems.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return items.get(oldItemPosition) == newItems.get(newItemPosition);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return items.get(oldItemPosition).id == newItems.get(newItemPosition).id;
        }
    }


}
