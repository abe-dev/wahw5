package com.abedev.wahw5;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by home on 06.09.2016.
 */
public class SelectableViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener {
    public interface ClicksListener {
        void onClick(SelectableViewHolder viewHolder, long itemId);
        void onLongClick(SelectableViewHolder viewHolder, long itemId);
    }
    public static final int VIEW_TYPE = 10001;
    private TextView tvTitle;
    private ImageView imageCheckbox;
    private long itemId = -1;
    private Drawable checkedDrawable;
    private Drawable uncheckedDrawable;
    private ClicksListener clicksListener;


    public SelectableViewHolder(View itemView, ClicksListener clicksListener) {
        super(itemView);
        tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
        imageCheckbox = (ImageView) itemView.findViewById(R.id.imgCheckbox);
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
        checkedDrawable = ContextCompat.getDrawable(itemView.getContext(),R.drawable.ic_check_box_black_24dp);
        uncheckedDrawable = ContextCompat.getDrawable(itemView.getContext(),R.drawable.ic_check_box_outline_blank_black_24dp);
        this.clicksListener = clicksListener;
    }

    public void setValue(String text, long itemId) {
        tvTitle.setText(text);
        this.itemId = itemId;
    }

    public void setSelection(boolean isInSelectMode, boolean isSelected) {
        itemView.setLongClickable(!isInSelectMode);
        imageCheckbox.setVisibility(isInSelectMode ? View.VISIBLE : View.GONE);
        if (isInSelectMode) {
            if (isSelected)
                imageCheckbox.setImageDrawable(checkedDrawable);
            else
                imageCheckbox.setImageDrawable(uncheckedDrawable);
        }
    }


    @Override
    public void onClick(View v) {
        clicksListener.onClick(this,itemId);
    }

    @Override
    public boolean onLongClick(View v) {
        clicksListener.onLongClick(this,itemId);
        return true;
    }

}
