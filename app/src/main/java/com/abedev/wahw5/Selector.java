package com.abedev.wahw5;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.SparseArray;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by home on 06.09.2016.
 */
public class Selector {


    private static final String KEY_IS_IN_SELECT_MODE = "Selector.isInSelectMode";
    private static final String KEY_SELECTED_ITEMS = "Selector.selectedItems";

    public interface Listener {
        void stateChanged();
        void selectionChanged(int selectedItemsCount);
    }
    List<Listener> listeners = new ArrayList<>();
    HashSet<Long> selectedItems = new HashSet<>();
    public boolean isInSelectMode() {
        return isInSelectMode;
    }

    private boolean isInSelectMode = false;

    public boolean toggleSelection(long itemId) {
        if (isInSelectMode)
        {
            boolean isSelected = selectedItems.contains(itemId);
            isSelected = !isSelected;
            if (isSelected)
                selectedItems.add(itemId);
            else
                selectedItems.remove(itemId);
            notifySelectionChanged();
            return isSelected;
        }
        else
            return false;
    }

    public boolean isSelected(long itemId) {
        return selectedItems.contains(itemId);
    }

    public void startSelection(long itemId) {
        if (!isInSelectMode) {
            isInSelectMode = true;
            toggleSelection(itemId);
            notifyStateChanged();

        }
    }

    private void notifySelectionChanged() {
        int count = selectedItems.size();
        for (Listener listener : listeners) {
            listener.selectionChanged(count);
        }
    }

    private void notifyStateChanged() {
        for (Listener listener : listeners) {
            listener.stateChanged();
        }
    }

    public void endSelection() {
        if (isInSelectMode) {
            selectedItems.clear();
            isInSelectMode = false;
            notifyStateChanged();
        }
    }

    public Set<Long> getSelectedItemsIds() {
        return selectedItems;
    }

    public void addListener(Listener listener) {
        if (!listeners.contains(listener))
            listeners.add(listener);
    }

    public void restoreState(Bundle savedInstanceState) {
        selectedItems.clear();
        isInSelectMode = savedInstanceState.getBoolean(KEY_IS_IN_SELECT_MODE);
        long[] items = savedInstanceState.getLongArray(KEY_SELECTED_ITEMS);
        for (long item : items) {
            selectedItems.add(item);
        }
        notifyStateChanged();
    }

    public void saveState(Bundle outState) {
        outState.putBoolean(KEY_IS_IN_SELECT_MODE,isInSelectMode);
        long[] items = new long[selectedItems.size()];
        int i = 0;
        for (Long selectedItem : selectedItems) {
            items[i++] = selectedItem;
        }
        outState.putLongArray(KEY_SELECTED_ITEMS,items);
    }
}
