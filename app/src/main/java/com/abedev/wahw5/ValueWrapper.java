package com.abedev.wahw5;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by biryuk on 08.09.2016.
 */
class ValueWrapper implements Parcelable {
    public final String value;
    public final long id;

    public ValueWrapper(String value, long id) {
        this.value = value;
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.value);
        dest.writeLong(this.id);
    }

    protected ValueWrapper(Parcel in) {
        this.value = in.readString();
        this.id = in.readLong();
    }

    public static final Creator<ValueWrapper> CREATOR = new Creator<ValueWrapper>() {
        @Override
        public ValueWrapper createFromParcel(Parcel source) {
            return new ValueWrapper(source);
        }

        @Override
        public ValueWrapper[] newArray(int size) {
            return new ValueWrapper[size];
        }
    };
}
